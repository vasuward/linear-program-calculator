#include "matrix.h"
#include <stdlib.h>

Matrix* MatrixCopy(const Matrix* m){
	if(m->theMatrix == NULL){
		return NULL;
	}
	Matrix* ret = malloc(sizeof(struct Matrix));
	ret->cols = m->cols;
	ret->rows = m->rows;

	ret->theMatrix = malloc(ret->cols * sizeof(double *));

	for(int i = 0; i < ret->cols; i++){
		ret->theMatrix[i] = malloc(ret->rows * sizeof(double));
		for(int j = 0; j < ret->rows; j++){
			ret->theMatrix[i][j] = m->theMatrix[i][j];
		}
	}
	return ret;
}

Matrix* freeMatrix(Matrix *m){
	if(m == NULL){
		return NULL;
	}
	for(int i = 0; i < m->cols; i++){
		free(m->theMatrix[i]);
	}
	free(m->theMatrix);
	if(m->theMatrix == NULL){
		return NULL;
	}else{
		return m;
	}
}

Matrix* addMatrices(const Matrix* lhs, const Matrix* rhs){
	if(lhs->cols != rhs->cols || lhs->rows != rhs->rows){
		return NULL;
	}

	Matrix* ret = MatrixCopy(lhs);

	for(int i = 0; i < ret->cols; i++){
		for(int j = 0; j < ret->rows; j++){
			ret->theMatrix[i][j] = lhs->theMatrix[i][j] + rhs->theMatrix[i][j];
		}
	}
	return ret;
}

Matrix* subMatrices(const Matrix* lhs, const Matrix* rhs){
	if(lhs->cols != rhs->cols || lhs->rows != rhs->rows){
		return NULL;
	}

	Matrix* ret = MatrixCopy(lhs);

	for(int i = 0; i < ret->cols; i++){
		for(int j = 0; j < ret->rows; j++){
			ret->theMatrix[i][j] = lhs->theMatrix[i][j] - rhs->theMatrix[i][j];
		}
	}
	return ret;
}

Matrix* multMatrices(const Matrix* lhs, const Matrix* rhs){
	if(lhs->cols != rhs->rows){
		return Null;
	}

	Matrix* ret = malloc(sizeof(struct Matrix));
	ret->cols = rhs->cols;
	ret->rows = lhs->rows;

	ret->theMatrix = malloc(ret->cols * sizeof(double *));

	for(int i = 0; i < ret->cols; i++){
		ret->theMatrix[i] = malloc(ret->rows * sizeof(double));
		for(int j = 0; j < ret->rows; j++){
			for (int k=0; k < ret->cols; k++){
				ret->theMatrix[i][j]= rhs->theMatrix[j][k] + lhs->theMatrix[k][i];
			}

		}
	}
	return ret;

}

Matrix* transposeMatrix(const Matrix* m){
	if(m->theMatrix == NULL){
		return NULL;
	}
	Matrix* ret = malloc(sizeof(struct Matrix));
	ret->cols = m->rows;
	ret->rows = m->cols;

	ret->theMatrix = malloc(ret->cols * sizeof(double *));

	for(int i = 0; i < ret->cols; i++){
		ret->theMatrix[i] = malloc(ret->rows * sizeof(double));
		for(int j = 0; j < ret->rows; j++){
			ret->theMatrix[i][j] = m->theMatrix[j][i];
		}
	}
	return ret;

}

//Helper function to delete element from array
void deleteArrayElement(double *arr, int size, int i){
	for(int j = i; j < size - 1; j++){
		arr[j] = arr[j+1];
	}
}

//Always deletes the first row, and deletes column c
Matrix* delRowAndColumn(const Matrix* m, int c){
	Matrix* ret= malloc(sizeof(struct Matrix));
	ret->cols = m->cols - 1;
	ret->rows = m->rows - 1;

	ret->theMatrix = malloc(ret->cols * sizeof(int *));


}

int MatrixDet(const Matrix* m){
	int s = 1;
	int det = 0;
	if ((m->rows == 1) && (m->cols == 1)){
		return m->theMatrix[0][0];
	}
	else {
		for (int i=0; i< m->cols; i++){
			Matrix* newMatrix= delRowAndColumn(m, 0, i);
			det= det + s*m->theMatrix[0][i]*MatrixDet(newMatrix);
			s= s*(-1);
		}
	}
	return det;
}

Matrix* inverseMatrix(const Matrix* m){
	Matrix* ret= malloc(sizeof(struct Matrix));
	ret->cols = m->cols ;
	ret->rows = m->rows ;
	ret->theMatrix = malloc(ret->cols * sizeof(double *));

	for (int i=0; i < ret->cols; i++){
		ret->theMatrix[i] = malloc(ret->rows * sizeof(double));
		int s= s * 1;
		for (int j=0; j < ret->rows; j++){
			Matrix* newm= delRowAndColumn(m,i,j);
			int val= MatrixDet(newm);
			ret->theMatrix[i][j]= s * val;
			s = s * -1;
		}
		s = s * -1;
	}

	Matrix* newMtrans= transposeMatrix(ret);
	double det= MatrixDet(m);
	for (int i=0; i<ret->cols; i++){
		for (int j=0; j<ret->rows; j++){
			newMtrans->theMatrix[i][j]= newMtrans->theMatrix[i][j]/det;
		}
	}

	return newMtrans;
}

//b is a vector; must be 1 by n
Matrix* getCols(const Matrix* a, const Matrix* b){
	Matrix* ret = malloc(sizeof(Matrix));
	ret->cols = b->rows;
	ret->rows = a->rows;
	
	ret->theMatrix = malloc(ret->cols * sizeof(double *));

	for(int i = 0; i < ret->cols; i++){
		ret->theMatrix[i] = malloc(ret->rows * sizeof(double));
		for(int j = 0; j < ret->rows; j++){
			ret->theMatrix[i][j] = m->theMatrix[b->theMatrix[0][i]][j];
		}
	}

	return ret;
}

bool isLEZero(const Matrix* m){
	for(int i = 0; i < m->cols; i++){
		for(int j = 0; j < m->rows; j++){
			if(m->theMatrix[i][j] <= 0){
				return true;
			}
		}
	}
	return false;
}

double findk(Matrix* c, Matrix* n){
	double *arr = n->theMatrix[0];
	for(int i = 0; i < n->rows; i++){
		for(int j = 0; j < n->rows; j++){
			if(arr[j] > arr[i]){
				double temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}

	double k;
	for(int l = 0; l < n->rows; l++){
		k = n->theMatrix[0][l];
		if(c->theMatrix[0][l] > 0){
			break;
		}
	}
	return k;
}


//basis,nonbasis is a 1xn matrix
double simplex(Matrix* c, Matrix* m, Matrix* b, double z, Matrix* basis, Matrix* nonbasis){
	// tranform LP into canonical form for basis B
	Matrix* Mb= getCols(m,basis)
	Matrix* y= multMatrices(transposeMatrix(inverseMatrix(Mb)),getCols(c,basis));
	z= addMatrices(multMatrices(transposeMatrix(y),b),z)[0][0];
	c= subMatrices(transposeMatrix(c),multMatrices(transposeMatrix(y),m));
	m= multMatrices(inverseMatrix(Mb),m);
	b= multMatrices(inverseMatrix(Mb),b);

	// create x
	Matrix* x = malloc(sizeof(struct Matrix));
	x->cols = 1;
	x->rows = m->cols;
	x->theMatrix = malloc(x->cols * sizeof(double))
	
	int j=0;
	for (int i=0; i<x->rows; i++){
		x->theMatrix[i] = malloc(x->rows * sizeof(double));
		if (i+1 == basis[j][0]) {
			x[0][i]= b[0][j]
		}
		else {
			x[0][i]= 0
		}
		j= j+1
	}

	// if cN<=0, x is optimal
	if isLEZero(getCols(c,nonbasis)){
		return x
	}

	// choose min k in nonbasis such that cK>0
	double k = findk(c,transposeMatrix(nonbasis))

	// if mk<=0 then p is unbounded
	Matrix* j = malloc(sizeof(struct Matrix));
	j->cols=1;
	j->rows=1;
	j->theMatrix= malloc(sizeof(double *));
	j->theMatrix[0][0]= k
	if (isLEZero(getCols(m,j))){
		print('P is Unbounded';)
		return
	}
	//choose t

	//recursion
	simplex(c,m,b,z,basis,nonbasis)
}
