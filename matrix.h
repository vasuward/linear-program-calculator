#ifndef MATRIX_H
#define MATRIX_H

struct Matrix {
	int cols;
	int rows;
	double* theMatrix[];
};

Matrix * MatrixCopy(const Matrix* m);

Matrix * freeMatrix(Matrix *m);

Matrix* addMatrices(const Matrix* lhs, const Matrix* rhs);

Matrix* subMatrices(const Matrix* lhs, const Matrix* rhs);

Matrix* multMatrices(const Matrix* lhs, const Matrix* rhs);

Matrix* transposeMatrix(const Matrix* m);

Matrix* inverseMatrix(const Matrix* m);

double MatrixDet(const Matrix* m);

Matrix* getCols(const Matrix* a, const Matrix* b);

bool isLEZero(const Matrix* m);

//Find a minimal k in n, s.t c<k> is greater that 0
double findk(Matrix* c, Matrix* n);

double MatrixDet(const Matrix* m);

double simplex(Matrix* c, Matrix* m, Matrix* b, double z, Matrix* basis, Matrix* nonbasis)

