#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
    int bufsize = 64;
    char buffer[bufsize];
    
    printf("Please enter an LP in the required form.\n");
    while(1){
        printf("Please enter y to begin");
        fgets(buffer, bufsize, stdin);
        if((strcmp(buffer, "y") == 0) || (strcmp(buffer, "Y") == 0)){
            break;
        }else{
            printf("You did not enter y");
        }
    }

    printf("Enter the number of values in c transpose");
    fgets(buffer, bufsize, stdin);
    int cLen = atof(buffer);
    Matrix c = {1, cLen};
    c->theMatrix = malloc(1 * sizeof(double *));
    c->theMatrix[0] = malloc(cLen * sizeof(double *));
    printf("Enter the values of c transpose, one by one. Follow each entry with an enter:\n");
    int i = 0;
    for(i = 0; i < cLen; i++){
        fgets(buffer, bufsize, stdin);
        c->theMatrix[0][i] = atof(buffer);
    }

    printf("Enter the number of values in z transpose");
    fgets(buffer, bufsize, stdin);
    int zLen = atof(buffer);
    Matrix z = {1, zLen};
    c->theMatrix = malloc(1 * sizeof(double *));
    c->theMatrix[0] = malloc(zLen * sizeof(double *));
    printf("Enter the values of z, one by one. Follow each entry with an enter:\n");
    int i = 0;
    for(i = 0; i < zLen; i++){
        fgets(buffer, bufsize, stdin);
        z->theMatrix[0][i] = atof(buffer);
    }

    printf("Enter the number of columns of constraints:");
    fgets(buffer, bufsize, stdin);
    int Acols = atoi(buffer);
    printf("Enter the number of rows of constraints");
    fgets(buffer, bufsize, stdin);
    int Arows = atoi(buffer);
    Matrix A = {Acols, Arows};
    A->theMatrix = malloc(A->cols * sizeof(double *));
    i = 0;
    int j = 0;
    for(;i < A->cols; i++){
        A->theMatrix[i] = malloc(A->rows * sizeof(double *));
    }

    i = 0;
    j = 0;
    for(; i < A->cols; i++){
        printf(Enter the values in column %d, one by one, seperated by an Enter, i);
        for(; j < A->rows; j++){
            fgets(buffer, bufsize, stdin);
            A->theMatrix[i][j] = atof(buffer);
        }
    }

    printf("Enter the number of values in the basis");
    fgets(buffer, bufsize, stdin);
    int bLen = atof(buffer);
    Matrix b = {1, bLen};
    b->theMatrix = malloc(1 * sizeof(double *));
    b->theMatrix[0] = malloc(cLen * sizeof(double *));
    printf("Enter the values of the basis, one by one. Follow each entry with an enter:\n");
    int i = 0;
    for(i = 0; i < bLen; i++){
        fgets(buffer, bufsize, stdin);
        b->theMatrix[0][i] = atof(buffer);
    }    
 }
